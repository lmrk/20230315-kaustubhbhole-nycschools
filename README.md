# NY School App
This is a Demo application that consumes some data from the NY school API here https://dev.socrata.com/foundry/data.cityofnewyork.us/s3k6-pzi2 it also fetches SAT average scores from here https://dev.socrata.com/foundry/data.cityofnewyork.us/f9bf-2cp4

The app has two screens, one is a to display a list all schools from NYC, tapping on a row will take you to a detail screen with some additional information Like SChool location on MapView, SAT score and Address and Contacts details. Tapping the website URL will take the user to safari, and tapping phone links will launch phone app.

## Structure
### MVVM Architecture -
The app is constructed completely in SwiftUI using Combine for Networking classes and a view model architecture. The view model is the bridge between the network layer and the view layer, handling state business logic as well.

## Unit Tests - 
Sample of Unit testing has been created to test business logic inside ViewModel which also involves Mocking WebService classes using Protocols. ViewModel and Networking layer has been properly separated using Protocols. 

## SAT Caching - 
A small quick mechanism written to cache SAT details in Memory for particular session only. if already SAT details availbale and user try to open same school details again SAT data API will not hit again.
   
## Screenshots 
![School List](https://gitlab.com/kbhole/20230315-kaustubhbhole-nycschools/-/blob/main/AppScreenShots/Schools.png)

![School Details](https://gitlab.com/kbhole/20230315-kaustubhbhole-nycschools/-/blob/main/AppScreenShots/Details.png)

![Unit Tests](https://gitlab.com/kbhole/20230315-kaustubhbhole-nycschools/-/blob/main/AppScreenShots/TestCases.png)
