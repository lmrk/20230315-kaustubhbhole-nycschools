//
//  NYCSchool.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import SwiftUI

@main
struct NYCSchoolApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolListView(viewModel: SchoolListViewModel())
        }
    }
}
