//
//  APICall.swift
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import Foundation

/// URL Request Builder
public protocol URlRequestBuilder {
    var endPoint: String { get }
    var method: String { get }
    var headers: [String: String]? { get }
    func body() throws -> Data?
}

/// API Errors
enum APIError: Swift.Error {
    case invalidURL
    case httpCode(HTTPCode)
    case unexpectedResponse
}

// Extension for API Error
extension APIError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL: return "Invalid URL"
        case let .httpCode(code): return "Unexpected HTTP code: \(code)"
        case .unexpectedResponse: return "Unexpected response from the server"
        }
    }
}

// Extension to returns UrlRequest
extension URlRequestBuilder {

    /// Method to create UrlRequest Object
    /// - Parameter baseURL: baseURL for Apis
    /// - Throws: Error if any
    /// - Returns: URLRequest Object
    func urlRequest(baseURL: String) throws -> URLRequest {
        var finalUrl: URL?
        let urlString = baseURL + endPoint
        finalUrl = URL(string: urlString)

        guard let url = finalUrl else {
            throw APIError.invalidURL
        }

        var request = URLRequest(url: url)
        request.httpMethod = method
        request.allHTTPHeaderFields = headers
        request.httpBody = try body()
        return request
    }
}

public typealias HTTPCode = Int
public typealias HTTPCodes = Range<HTTPCode>

/// Http Codes
extension HTTPCodes {
    public static let success = 200 ..< 300
}
