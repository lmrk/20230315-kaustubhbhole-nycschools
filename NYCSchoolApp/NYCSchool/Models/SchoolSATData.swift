//
//  SchoolSATData.swift
//  Template
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import Foundation

/// School SAT Data Model
struct SchoolSATData: Decodable {
    let dbn: String
    let schoolName: String?
    let numOfSatTestTakers: String?
    let satCriticalReadingAvgScore: String?
    let satMathAvgScore: String?
    let satWritingAvgScore: String?

    /// Coding Keys for Decoding 
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}
