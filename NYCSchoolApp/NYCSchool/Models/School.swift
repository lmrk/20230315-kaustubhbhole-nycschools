//
//  SchoolSATDataModel.swift
//  Template
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import Foundation

/// School Data Model
struct School: Decodable {
    let dbn: String
    let schoolName: String?
    let overviewParagraph: String?
    let school10thSeats: String?
    let academicOpportunities1: String?
    let academicOpportunities2: String?
    let location: String?
    let phoneNumber: String?
    let faxNumber: String?
    let schoolEmail: String?
    let website: String?
    let finalgrades: String?
    let totalStudents: String?
    let extracurricularActivities: String?
    let schoolSports: String?
    let attendanceRate: String?
    let primaryAddressLine1: String?
    let city: String?
    let zip: String?
    let stateCode: String?
    let latitude: String?
    let longitude: String?
    let nta: String?

    /// Coding Keys for Decoding
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case school10thSeats = "school_10th_seats"
        case academicOpportunities1 = "academicopportunities1"
        case academicOpportunities2 = "academicopportunities2"
        case location
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website
        case finalgrades
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
        case attendanceRate = "attendance_rate"
        case primaryAddressLine1 = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        case latitude
        case longitude
        case nta
    }
}

// Extension created to generate complete address
extension School {
    /// Function will returns Complete Address
    /// - Returns: String
    func getCompleteAddress() -> String {
        var completeAddress = ""
        if let primaryAddress = self.primaryAddressLine1 {
            completeAddress.append("\(primaryAddress)")
        }
        if let city = self.city {
            completeAddress.append(", \(city)")
        }
        if let state = self.stateCode {
            completeAddress.append(", \(state)")
        }
        if let zip = self.zip {
            completeAddress.append(", \(zip)")
        }
        return completeAddress
    }
}
