//
//  SchoolListView+LocalizableStrings.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import Foundation

// Localizable Strings
extension SchoolListView {

    /// Enum to hold list of Localizable Strings required for School List Screen
    enum LocalizableStrings: String {
        case screenTitle = "NYC Schools"

        var description: String {
            self.rawValue
        }
    }
}
