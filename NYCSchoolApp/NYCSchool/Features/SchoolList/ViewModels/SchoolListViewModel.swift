import Foundation
import Combine

class SchoolListViewModel: ObservableObject {

    // MARK: Variables -
    @Published var schools: [SchoolViewModel] = []
    var selectedSchool: SchoolViewModel?
    var cancellable: AnyCancellable?
    var webService: NYCSchoolWebServiceProtocol

    // MARK: - Initializer -
    /// - Parameter historyService: historyService
    init(nycSchoolWebService: NYCSchoolWebServiceProtocol = NYCSchoolWebService()) {
        self.webService = nycSchoolWebService
    }

    /// Function to Create object of SchoolViewModel from School Display NA in case of missing Data
    /// - Parameter schools: School decodable object received from API
    /// - Returns: SchoolViewModel used for displaying Data on View
    func createSchoolViewModels(schools: [School]) -> [SchoolViewModel] {
        let schoolViewModels = schools.map { school in
            return SchoolViewModel(
                schoolName: school.schoolName ?? "NA",
                email: school.schoolEmail ?? "NA",
                website: school.website ?? "NA",
                phone: school.phoneNumber ?? "NA",
                address: school.getCompleteAddress(),
                dbn: school.dbn,
                latitude: school.latitude ?? "NA",
                longitude: school.longitude ?? "NA",
                overviewParagraph: school.overviewParagraph ?? "NA"
            )
        }
        return schoolViewModels
    }

    /// Create SchoolDetailsViewModel
    /// - Parameter school: SchoolViewModel object
    /// - Returns: SchoolDetailsViewModel object
    func getSchoolDetailsViewModel(from school: SchoolViewModel) -> SchoolDetailsViewModel {
        return SchoolDetailsViewModel(school: school)
    }
}

extension SchoolListViewModel {
    func fetchNYCSchoolsFromAPI(completion: @escaping (Bool) -> Void) {
        self.cancellable = self.webService.fetchNYCSchools()
            .mapError({ (error) -> Error in
                GlobalLogger.sharedLogger.send(error.localizedDescription, logLevel: .error)
                return error
            })
            .sink(receiveCompletion: { _ in

            }, receiveValue: { schools in
                var schoolArray = [School]()
                schoolArray.append(contentsOf: schools)
                self.schools = self.createSchoolViewModels(schools: schoolArray)
            })
    }
}
