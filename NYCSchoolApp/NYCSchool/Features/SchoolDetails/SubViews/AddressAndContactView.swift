//
//  AddressAndContactView.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import SwiftUI

/// Struct to displsy School Address and other Contact details
struct AddressAndContactView: View {

    // MARK: Variables =
    var schoolViewModel: SchoolViewModel

    // MARK: View Body -
    var body: some View {
        RoundedRectCardComponent {
            VStack (alignment: .leading) {
                SectionHeaderTextView(text: LocalizableStrings.addressAndContact.description)
                PrimaryTextView(text: schoolViewModel.address)
                SecondaryTextView(text: schoolViewModel.email)
                SecondaryTextView(text: schoolViewModel.phone)
                    .onTapGesture {
                        PhoneNumberUtil.dial(phoneNumber: schoolViewModel.phone)
                    }
                SecondaryTextView(text: schoolViewModel.website)
                    .foregroundColor(.blue)
                    .onTapGesture {
                        let urlString = "https://" + schoolViewModel.website
                        guard let url = URL(string: urlString) else {
                            GlobalLogger.sharedLogger.send("Looks like Incorrect URL passed. Please check once", logLevel: .error)
                            return
                        }
                        UIApplication.shared.open(url)
                    }
            }
        }
        .padding(.horizontal, Constants.standardHorizontalPadding)
        .padding(.vertical, Constants.standardVerticalPadding)
    }
}

extension AddressAndContactView {
    /// Enum to hold list of Localizable Strings required for School List Screen
    enum LocalizableStrings: String {
        case addressAndContact = "Address and Contact"

        var description: String {
            self.rawValue
        }
    }
}

/// Previews
struct AddressAndContactView_Previews: PreviewProvider {
    static var previews: some View {
        AddressAndContactView(schoolViewModel: SchoolViewModel(schoolName: "NYC School",
                                                         email: "abc@test.com",
                                                         website: "www.abc.com",
                                                         phone: "545465",
                                                         address: "",
                                                         dbn: "",
                                                         latitude: "",
                                                         longitude: "",
                                                         overviewParagraph: ""))
    }
}
