//
//  SchoolDetailsView.swift
//  Template
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import SwiftUI
import MapKit

/// Struct to Display School Details Information 
struct SchoolDetailsView: View {

    // MARK: Variables -
    @State var countryRegion: MKCoordinateRegion?
    @State var region: MKCoordinateRegion = MKCoordinateRegion()
    @ObservedObject var viewModel: SchoolDetailsViewModel

    // MARK: View Body -
    var body: some View {
        ScrollView {
            overviewSectionView
            if self.viewModel.schoolSATDetails != nil {
                satDetailsView
            }
            Spacer().frame(height: Constants.standardVerticalPadding)
            AddressAndContactView(schoolViewModel: viewModel.school)
            MapSectionView(school: viewModel.school)
        }
        .navigationTitle(viewModel.school.schoolName)
        .onLoad {
            viewModel.fetchSATInformation { isAPISuccess in
                GlobalLogger.sharedLogger.send("Fetch School SAT API Success", logLevel: .info)
            }
        }
    }

    /// SAT Details View
    @ViewBuilder var satDetailsView: some View {
        VStack (alignment: .leading) {
            SectionHeaderTextView(text: SchoolDetailsViewModel.LocalizableStrings.satDetailsText.description)
            RoundedRectCardComponent {
                VStack (alignment: .leading) {
                    PrimaryTextView(text: viewModel.getTextToShowNumberOfSatTestTakers())
                    PrimaryTextView(text: viewModel.getTextToDisplayAverageMathScore())
                    PrimaryTextView(text: viewModel.getTextToDisplayAverageReadingScore())
                    PrimaryTextView(text: viewModel.getTextToDisplayAverageWritingScore())
                }
            }
        }
        .padding(.horizontal, Constants.standardHorizontalPadding)
    }

    /// School Overview Paragraph Section View
    var overviewSectionView: some View {
        VStack (alignment: .leading) {
            SectionHeaderTextView(text: "School Overview")
            PrimaryTextView(text: viewModel.school.overviewParagraph)
        }
        .padding(.horizontal, Constants.standardHorizontalPadding)
        .padding(.vertical, Constants.standardVerticalPadding)
    }
}
