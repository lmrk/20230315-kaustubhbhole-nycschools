//
//  SchoolDetailsViewModel+LocalizableStrings.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import Foundation

// Localizable Strings
extension SchoolDetailsViewModel {

    /// Enum to hold list of Localizable Strings required for School Details Screen
    enum LocalizableStrings: String {
        case schoolNameNotAvailable = "School"
        case satDetailsText = "SAT Details: "
        case numberOfSATTestTakers = "Number of Sat test takers: "
        case averageReadingScore = "Average critical reading score: "
        case averageWritingScore = "Average writing score: "
        case averageMathScore = "Average Math score: "

        var description: String {
            self.rawValue
        }
    }
}
