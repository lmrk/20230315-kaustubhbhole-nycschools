//
//  SchoolDetailsViewModel.swift
//  Template
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import Foundation
import Combine

class SchoolDetailsViewModel: ObservableObject {

    // MARK: Variables -
    var cancellable: AnyCancellable?
    var webService: NYCSchoolWebServiceProtocol
    @Published var school: SchoolViewModel
    @Published var schoolSATDetails: SchoolSATData?

    // MARK: - Initializer -
    /// - Parameter historyService: historyService
    init(school: SchoolViewModel, nycSchoolWebService: NYCSchoolWebServiceProtocol = NYCSchoolWebService()) {
        self.school = school
        self.webService = nycSchoolWebService
    }

    func getTextToShowNumberOfSatTestTakers() -> String {
        return "\(LocalizableStrings.numberOfSATTestTakers.description + (schoolSATDetails?.numOfSatTestTakers ?? "NA"))"
    }

    func getTextToDisplayAverageReadingScore() -> String {
        return "\(LocalizableStrings.averageReadingScore.description + (schoolSATDetails?.satCriticalReadingAvgScore ?? "NA"))"
    }

    func getTextToDisplayAverageWritingScore() -> String {
        return "\(LocalizableStrings.averageWritingScore.description + (schoolSATDetails?.satWritingAvgScore ?? "NA"))"
    }

    func getTextToDisplayAverageMathScore() -> String {
        return "\(LocalizableStrings.averageMathScore.description + (schoolSATDetails?.satMathAvgScore ?? "NA"))"
    }
}

extension SchoolDetailsViewModel {
    /// Function to fetch SAT Information from API
    func fetchSATInformation(completion: @escaping (Bool) -> Void) {

        // Check if School SAT data already present for this selected School then show it else Proceed for API call
        if self.school.schoolSATData != nil {
            self.schoolSATDetails = self.school.schoolSATData
            return
        }

        self.cancellable = self.webService.fetchSchoolSATDetails(dbn: school.dbn)
            .mapError({ (error) -> Error in
                GlobalLogger.sharedLogger.send(error.localizedDescription, logLevel: .error)
                completion(false)
                return error
            })
            .sink(receiveCompletion: { _ in

            }, receiveValue: { scholSATDetails in
                completion(true)
                self.schoolSATDetails = scholSATDetails.first
                self.school.schoolSATData = self.schoolSATDetails
            })
    }
}
