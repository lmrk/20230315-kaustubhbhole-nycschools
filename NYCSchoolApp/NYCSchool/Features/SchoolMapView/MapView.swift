import SwiftUI
import MapKit


/// MapView class which creates a MapView instance using UIKit and it is accessible as a SwiftUI View
struct MapView: UIViewRepresentable {

    // MARK: Variables -
    let latitude: CLLocationDegrees
    let longitude: CLLocationDegrees

    // Protocol Methods -
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        mapView.delegate = context.coordinator

        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location

        let initialRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: latitude, longitude: longitude),
                                               span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        mapView.setRegion(initialRegion, animated: true)
        mapView.addAnnotation(annotation)
        mapView.setCenter(location, animated: true)

        return mapView
    }

    func updateUIView(_ mapView: MKMapView, context: Context) {
        // do nothing
    }

    func makeCoordinator() -> Coordinator {
        Coordinator()
    }

    /// Coordinator Class to implement some Delegates 
    class Coordinator: NSObject, MKMapViewDelegate {
        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            let identifier = "Pin"

            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView == nil {
                annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = false
            } else {
                annotationView?.annotation = annotation
            }

            return annotationView
        }
    }
}
