//
//  MapSectionView.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import SwiftUI

/// Strucvt to create Map section View -
struct MapSectionView: View {

    // MARK: Variables -
    var school: SchoolViewModel

    // MARK: View Body -
    var body: some View {
        RoundedRectCardComponent {
            if let latitude = school.latitude, let latitudeInDouble = Double(latitude),
               let longitude = school.longitude, let longitudeInDouble = Double(longitude) {
                MapView(latitude: latitudeInDouble, longitude: longitudeInDouble)
                    .frame(height: 100)
                    .edgesIgnoringSafeArea(.all)
            }
        }
        .padding(.horizontal, Constants.standardHorizontalPadding)
    }
}

/// Preview
struct MapSectionView_Previews: PreviewProvider {
    static var previews: some View {
        MapSectionView(school: SchoolViewModel(schoolName: "NYC School",
                                               email: "abc@test.com",
                                               website: "www.abc.com",
                                               phone: "545465",
                                               address: "",
                                               dbn: "",
                                               latitude: "",
                                               longitude: "",
                                               overviewParagraph: ""))
    }
}
