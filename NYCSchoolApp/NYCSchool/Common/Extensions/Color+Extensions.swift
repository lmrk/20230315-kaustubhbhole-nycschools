//
//  Color+Extensions.swift
//  Template
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import SwiftUI

extension Color {

    /// These are Colors defined in the Deloitte Dot template
    static let darkTextPrimary = Color(UIColor(named: "DarkText-Primary")!)
    static let darkTextSecondary = Color(UIColor(named: "DarkText-Secondary")!)
    static let neutralBlack = Color(UIColor(named: "Neutral-Black")!)
    static let neutralWhite = Color(UIColor(named: "Neutral-White")!)
    static let neutralDarkerClay = Color(UIColor(named: "Neutral-DarkerClay")!)
    static let neutralDarkClay = Color(UIColor(named: "Neutral-DarkClay")!)
    static let neutralClay = Color(UIColor(named: "Neutral-Clay")!)
    static let neutralLightClay = Color(UIColor(named: "Neutral-LightClay")!)
    static let neutralLighterClay = Color(UIColor(named: "Neutral-LighterClay")!)
    static let errorRed = Color(UIColor(named: "Error-Red")!)
}

