//
//  PrimaryTextView.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import SwiftUI

struct PrimaryTextView: View {
    var text: String

    var body: some View {
        Text(text)
            .padding(.vertical, 1.0)
            .font(Font.bodyMedium)
            .foregroundColor(Color.neutralDarkClay)
    }
}

struct PrimaryTextView_Previews: PreviewProvider {
    static var previews: some View {
        PrimaryTextView(text: "Primary Text")
    }
}
