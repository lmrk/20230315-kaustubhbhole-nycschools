//
//  RoundedRectCardComponent.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import Foundation
import SwiftUI

struct RoundedRectCardComponent<Content: View>: View {
    let content: Content

    init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }

    var body: some View {
        content
            .modifier(RoundedCornerModifier())
    }
}
