//
//  SecondaryTextView.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import SwiftUI

struct SecondaryTextView: View {
    var text: String

    var body: some View {
        Text(text)
            .padding(.vertical, 1.0)
            .font(Font.bodyMedium)
            .foregroundColor(.blue)
    }
}

struct SecondaryTextView_Previews: PreviewProvider {
    static var previews: some View {
        SecondaryTextView(text: "Testing text")
    }
}
