//
//  RoundedCornerModifier.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 14/03/23.
//

import Foundation
import SwiftUI

struct RoundedCornerModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity,
                   alignment: .leading)
            .padding(.horizontal, 12.0)
            .padding(.vertical, 12.0)
            .background(.clear)
            .cornerRadius(5)
            .overlay(
                RoundedRectangle(cornerRadius: 5)
                    .stroke(.gray, lineWidth: 1)
            )
    }
}
