//
//  SectionHeaderTextView.swift
//  NYCSchool
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import SwiftUI

struct SectionHeaderTextView: View {

    // MARK: Variables -
    let text: String

    // MARK: View Body -
    var body: some View {
        Text(text)
            .bold()
            .foregroundColor(Color.neutralDarkClay)
            .font(Font.bodyMediumStrong)
            .padding(.bottom, 5.0)
    }
}

struct SectionHeaderTextView_Previews: PreviewProvider {
    static var previews: some View {
        SectionHeaderTextView(text: "Hello")
    }
}
