//
//  MockWebServices.swift
//  NYCSchoolTests
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import Foundation
import Combine
@testable import NYCSchool

struct MockNYCSchoolForSuccessResponse: NYCSchoolWebServiceProtocol {

    var baseURL: String = "Base_url"

    func fetchNYCSchools() -> AnyPublisher<[School], Error> {
        return AnyPublisher(Just([School(dbn: "test",
                                         schoolName: "NYC School",
                                         overviewParagraph: "Test info", school10thSeats: nil, academicOpportunities1: nil, academicOpportunities2: nil, location: nil, phoneNumber: "1234567789", faxNumber: nil, schoolEmail: "acb@testmail.com", website: "www.test.com", finalgrades: nil, totalStudents: "150", extracurricularActivities: nil, schoolSports: nil, attendanceRate: nil, primaryAddressLine1: "test Address", city: "New York", zip: "65432", stateCode: "NY", latitude: "12.34567", longitude: "45.98765", nta: nil)]))
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }

    func fetchSchoolSATDetails(dbn: String) -> AnyPublisher<[SchoolSATData], Error> {
        return AnyPublisher(Just([SchoolSATData(dbn: "test", schoolName: "NYC School", numOfSatTestTakers: "20", satCriticalReadingAvgScore: "80", satMathAvgScore: "85", satWritingAvgScore: "90")]))
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}

/// Mock class created to provide dummy success response for api calls
struct MockNYCSchoolForFailureResponse: NYCSchoolWebServiceProtocol {

    var baseURL: String = "Base_url"
    func fetchNYCSchools() -> AnyPublisher<[School], Error> {
        return URLSession.shared
            .dataTaskPublisher(for: URL(string: baseURL)!)
            .tryMap { data, response in
                throw APIError.unexpectedResponse
            }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }

    func fetchSchoolSATDetails(dbn: String) -> AnyPublisher<[SchoolSATData], Error> {
        return URLSession.shared
            .dataTaskPublisher(for: URL(string: baseURL)!)
            .tryMap { data, response in
                throw APIError.unexpectedResponse
            }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    /// Api Error Enum for Mocking
    enum APIError: Error {
        case invalidURL
        case httpCode(HTTPCode)
        case unexpectedResponse
    }
}
