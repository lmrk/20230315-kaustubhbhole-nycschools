//
//  SchoolDetailsViewModel.swift
//  NYCSchoolTests
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import Foundation
import XCTest
@testable import NYCSchool

class SchoolDetailsViewModelTests: XCTestCase {
    func testFetchNYCSchoolSATDetails() {
        let viewModel = SchoolDetailsViewModel(school: SchoolViewModel(schoolName: "",
                                                                       email: "",
                                                                       website: "",
                                                                       phone: "",
                                                                       address: "",
                                                                       dbn: "",
                                                                       latitude: "", longitude: "", overviewParagraph: ""), nycSchoolWebService: MockNYCSchoolForSuccessResponse())

        viewModel.fetchSATInformation { isSuccess in
            XCTAssertTrue(isSuccess)
        }
    }

    func testFetchNYCSchoolSATDetailsReturnsFailure() {
        let viewModel = SchoolDetailsViewModel(school: SchoolViewModel(schoolName: "",
                                                                       email: "",
                                                                       website: "",
                                                                       phone: "",
                                                                       address: "",
                                                                       dbn: "",
                                                                       latitude: "", longitude: "", overviewParagraph: ""), nycSchoolWebService: MockNYCSchoolForFailureResponse())

        viewModel.fetchSATInformation { isSuccess in
            XCTAssertFalse(isSuccess)
            XCTAssertNil(viewModel.schoolSATDetails)
        }
    }
}
