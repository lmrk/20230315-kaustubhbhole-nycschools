//
//  SchoolListViewModelTests.swift
//  NYCSchoolTests
//
//  Created by Bhole, Kaustubh Satish on 15/03/23.
//

import XCTest
@testable import NYCSchool

class SchoolListViewModelTests: XCTestCase {
    func testFetchNYCSchoolsReturnsSuccess() {
        let viewModel = SchoolListViewModel(nycSchoolWebService: MockNYCSchoolForSuccessResponse())

        viewModel.fetchNYCSchoolsFromAPI { isSuccess in
            XCTAssertTrue(isSuccess)
            XCTAssertNotNil(viewModel.schools.first)
            XCTAssertNotNil(viewModel.schools.first?.dbn)
        }
    }

    func testFetchNYCSchoolsReturnsFailure() {
        let viewModel = SchoolListViewModel(nycSchoolWebService: MockNYCSchoolForFailureResponse())

        viewModel.fetchNYCSchoolsFromAPI { isSuccess in
            XCTAssertFalse(isSuccess)
            XCTAssertNil(viewModel.schools.first)
            XCTAssertNil(viewModel.schools.first?.dbn)
        }
    }
}
